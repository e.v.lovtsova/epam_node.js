import { NextFunction, Request, Response } from 'express';
import { GenreModel } from '../models/Genre';
import { validationResult } from 'express-validator';
import CustomError from '../utils/customError';

export const getGenres = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const genres = await GenreModel.find();
        res.status(200).json(genres);      
    } catch (err) {
        next(err);
    }
};

export const postAddGenre = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const newGenre = req.body;        
        const errors = validationResult(req);    

        if (!errors.isEmpty()) {
            const error = new CustomError(422, 'Validation failed, ' + errors.array()[0].msg);
            throw error;
        }

        const genre = new GenreModel(newGenre);
        await genre.save();
        res.status(201).json(genre);
    } catch (err) {
        next(err);
    }
};

export const getGenreById = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const genreId = req.params.genreId;
        const genre = await GenreModel.findById(genreId);

        if (!genre) {
            const error = new CustomError(404, 'Genre not found');
            throw error;
        }
        res.status(200).json(genre);
    } catch (err) {
        next(err);
    }
};

export const postUpdateGenre = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const updatedGenre = req.body;
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            const error = new CustomError(422, 'Validation failed, ' + errors.array()[0].msg);
            throw error;
        }

        await GenreModel.findByIdAndUpdate(updatedGenre.genreId, updatedGenre);
        const genre = await GenreModel.findById(updatedGenre.genreId);

        if (!genre) {
            const error = new CustomError(404, 'Genre not found');
            throw error;
        }

        res.status(200).json(genre);
    } catch (err) {
        next(err);
    }
};

export const postDeleteGenre = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const deletedGenre = await GenreModel.findByIdAndRemove(req.body.genreId);
        
        if (!deletedGenre) {
            const error = new CustomError(404, 'Genre not found');
            throw error;
        }
        
        res.status(200).json('Genre successfully deleted');
    } catch (err) {
        next(err);  
    }
};
