import { NextFunction, Request, Response } from 'express';
import { MovieModel } from '../models/Movie';
import { validationResult } from 'express-validator';
import CustomError from '../utils/customError';
import { GenreModel } from '../models/Genre';

export const getMovies = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const movies = await MovieModel.find().populate('genres');
        res.status(200).json(movies);
    } catch (err) {
        next(err);
    }
};

export const postAddMovie = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const movie = req.body;
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            const error = new CustomError(422, 'Validation failed, ' + errors.array()[0].msg);
            throw error;
        }
    
        const newMovie = new MovieModel(movie);
        await newMovie.save();
        res.status(201).json(newMovie);
    } catch (err) {
        next(err);
    }       
};

export const getMovieById = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const movieId = req.params.movieId;
        const movie = await MovieModel.findById(movieId).populate('genres');

        if (!movie) {
            const error = new CustomError(404, 'Could not find movie.');
            throw error;
        }
        res.status(200).json(movie);
    } catch (err) {
        next(err);
    }
};

export const postUpdateMovie = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const updatedMovie = req.body;
        const errors = validationResult(req);   
        if(!errors.isEmpty()) {
            const error = new CustomError(422, 'Validation failed, ' + errors.array()[0].msg);
            throw error;
        }

        await MovieModel.findByIdAndUpdate(updatedMovie.movieId, updatedMovie);
        const movie = await MovieModel.findById(updatedMovie.movieId);
        if (!movie) {
            const error = new CustomError(404, 'Could not find movie.');
            throw error;
        }
        res.status(200).json(movie);
    } catch (err) {
        next(err);
    }       
};

export const postDeleteMovie = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const movieId = req.body.movieId;
        const deletedMovie = await MovieModel.findByIdAndRemove(movieId);

        if (!deletedMovie) {
            const error = new CustomError(404, 'Could not find movie.');
            throw error;
        }        
        res.status(200).json('Movie successfully deleted');
    } catch (err) {
        next(err);
    }        
};

export const getMovieByGenre = async (req: Request, res: Response, next: NextFunction) => {
    try {     
        const genreId = req.params.genreId;
        const genre = await GenreModel.findById(genreId);

        if (!genre) {
            const error = new CustomError(404, 'Selected genre was not found');
            throw error;
        }

        const filteredMovies = await MovieModel
            .find({genres: genreId})
            .populate('genres'); 

        if(filteredMovies.length === 0) {
            return res.status(200).json('There are no movies of selected genre at the moment');
        }        
        res.status(200).json(filteredMovies);  
    } catch (err) {
        next(err);
    }
};