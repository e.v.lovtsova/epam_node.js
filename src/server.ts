import mongoose from 'mongoose';
import dotenv from 'dotenv';
import app from './app';

dotenv.config();
const PORT = 3000; 
const uri = process.env.DB_URI;

if (!uri) {
    console.error('DB_URI is not defined in the environment variables');
    process.exit(1);
}

mongoose.connect(uri).then(() => {
    app.listen(PORT, () => {
        console.log(`Server listening on port ${PORT}, MongoDB connected`);
    });
}).catch(err => {
    console.log(`MongoDB connection error. Please make sure MongoDB is running. ${err}`);    
});