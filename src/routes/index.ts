import express from 'express';
import movieRoutes from './movie-routes';
import genreRoutes from './genre-routes';

const router = express.Router();

// middleware to parse JSON request bodies
router.use(express.json());
// middleware to parse URL-encoded request bodies
router.use(express.urlencoded({extended: true}));

router.use('/movies', movieRoutes);
router.use('/genres', genreRoutes);

/**
 * @openapi
 * /:
 *   get:
 *     tags:
 *      - Main
 *     summary: Returns a welcome message
 *     responses:
 *      '200':
 *         description: Welcome to home page!
 *      '500':
 *         description: Internal Server Error
 */
router.get('/', (req, res) => { 
    res.status(200).json({ message: 'Welcome to home page!'}); 
}); 

/**
 * @openapi
 * /health-check:
 *   get:
 *     tags:
 *      - Main
 *     summary: Check server health status
 *     responses:
 *      '200':
 *         description: Server is running
 *      '500':
 *         description: Internal Server Error
 */
router.get('/health-check', (req, res) => { 
    res.status(200).json({ message: 'Server is running' }); 
}); 

export default router;