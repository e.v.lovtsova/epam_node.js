import express from 'express';
import { getGenreById, getGenres, postAddGenre, postDeleteGenre, postUpdateGenre } from '../controllers/genre-controllers';
import { body } from 'express-validator';

const genreRoutes = express.Router();


/**
 * @openapi
 * /genres:
 *   get:
 *     tags:
 *      - Genres
 *     summary: Returns an array of available genres
 *     responses:
 *       200:
 *         description: An array of available genres
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                  $ref: '#/components/schemas/Genre'
 *       '500':
 *         description: Internal Server Error
 */
genreRoutes.get('/', getGenres);


/**
 * @openapi
 * /genres/add-genre:
 *   post:
 *     tags:
 *      - Genres
 *     summary: Saves new genre into the db
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *               description:
 *                 type: string
 *     responses:
 *      '201':
 *         description: Ok
 *         content:
 *             application/json:
 *                 schema:
 *                     $ref: '#/components/schemas/Genre'
 *      '422':
 *          description: Validation failed
 *      '500':
 *         description: Internal Server Error
 */
genreRoutes.post(
    '/add-genre',
    [
        body('name')
            .trim()
            .notEmpty().withMessage('There should be a name of genre')
            .isString().withMessage('Invalid name of genre'),
        body('description')
            .trim()
            .notEmpty().withMessage('There should be a description')
            .isLength({min: 10}).withMessage('The length of description should be at least 10 charecters long')
    ],
    postAddGenre);

/**
 * @openapi
 * /genres/{genreId}:
 *   get:
 *     tags:
 *      - Genres
 *     summary: Returns specific genre by its Id
 *     parameters:
 *      - in: path
 *        name: genreId
 *        schema:
 *          type: string
 *        required: true
 *        description: ID of the genre
 *     responses:
 *       '200':
 *         description: Ok
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Genre'
 *       '404':
 *          description: Genre not found
 *       '500':
 *         description: Internal Server Error
 */
genreRoutes.get('/:genreId', getGenreById);

/**
 * @openapi
 * /genres/update-genre:
 *   post:
 *     tags:
 *      - Genres
 *     summary: Saves updated genre into the db
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               genreId:
 *                 type: string
 *               name:
 *                 type: string
 *               description:
 *                 type: string
 *     responses:
 *      '200':
 *         description: Ok
 *         content:
 *          application/json:
 *              schema:
 *                  $ref: '#/components/schemas/Genre'
 *      '422':
 *          description: Validation Error
 *      '404':
 *          description: Genre not found
 *      '500':
 *         description: Internal Server Error
 */
genreRoutes.post(
    '/update-genre',
    [
        body('name')
            .trim()
            .notEmpty().withMessage('There should be a name of genre')
            .isString().withMessage('Invalid name of genre'),
        body('description')
            .trim()
            .notEmpty().withMessage('There should be a description')
            .isLength({min: 10}).withMessage('The length of description should be at least 10 charecters long')
    ],
    postUpdateGenre);
    
/**
 * @openapi
 * /genres/delete-genre:
 *   post:
 *     tags:
 *      - Genres
 *     summary: Delete genre by Id
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               genreId:
 *                 type: string
 *     description: ID of the genre to delete
 *     responses:
 *       '200':
 *         description: Ok
 *         content:
 *          text/plain:
 *             schema:
 *                type: string
 *                example: Genre successfully deleted
 *  
 *       '404':
 *         description: Page not found
 *       '500':
 *         description: Internal Server Error
 */
genreRoutes.post('/delete-genre', postDeleteGenre);

export default genreRoutes;