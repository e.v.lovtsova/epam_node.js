import express from 'express';
import { getMovieByGenre, getMovieById, getMovies, postAddMovie, postDeleteMovie, postUpdateMovie } from '../controllers/movie-controllers';
import { body } from 'express-validator';

const movieRoutes = express.Router(); 

/**
 * @openapi
 * /movies:
 *   get:
 *     tags:
 *      - Movies
 *     summary: Returns an array of available movies 
 *     responses:
 *       200:
 *         description: Ok
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                  $ref: '#/components/schemas/Movie'
 *       '500':
 *         description: Internal Server Error
 */
movieRoutes.get('/', getMovies);

/**
 * @openapi
 * /movies/add-movie:
 *   post:
 *     tags:
 *      - Movies
 *     summary: Saves new movie into the db
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               title:
 *                 type: string
 *               description:
 *                 type: string
 *               release:
 *                 type: string
 *                 format: date
 *               genres:
 *                 type: array
 *                 items:
 *                   $ref: '#/components/schemas/Genre'
 *     responses:
 *      '201':
 *         description: ok
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#components/schemas/Movie'
 *      '422':
 *         description: Validation failed
 *      '500':
 *         description: Internal Server Error
 */
movieRoutes.post(
    '/add-movie',
    [
        body('title')
            .trim()
            .notEmpty().withMessage('There should be a movie title'),
        body('genres')
            .isArray({ min: 1 }).withMessage('Please, select at least one movie genre')
            .notEmpty().withMessage('Please, select a movie genre'),
        body('release')
            .notEmpty().withMessage('Please, select a release date')
            .isDate(),
        body('description')
            .trim()
            .notEmpty().withMessage('There should be a movie description')
            .isLength({min: 10}).withMessage('The length of description should be at least 10 charecters long')
    ],
    postAddMovie);

/**
 * @openapi
 * /movies/{movieId}:
 *   get:
 *     tags:
 *      - Movies
 *     summary: Returns specific movie by its Id
 *     parameters:
 *      - in: path
 *        name: movieId
 *        schema:
 *          type: string
 *        required: true
 *        description: ID of the movie
 *     responses:
 *       200:
 *         description: Ok
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#components/schemas/Movie'
 *       '404':
 *          description: Movie not found
 *       '500':
 *         description: Internal Server Error
 */
movieRoutes.get('/:movieId', getMovieById);

/**
 * @openapi
 * /movies/update-movie:
 *   post:
 *     tags:
 *      - Movies
 *     summary: Saves updated movie into the db
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               movieId:
 *                 type: string
 *               title:
 *                 type: string
 *               description:
 *                 type: string
 *               release:
 *                 type: string
 *                 format: date
 *               genres:
 *                 type: array
 *                 items:
 *                   $ref: '#/components/schemas/Genre'
 *     responses:
 *      '201':
 *         description: Ok
 *         content:
 *          application/json:
 *             schema:
 *               $ref: '#/components/schemas/Movie'
 *      '404':
 *         description: Validation failed
 *      '500':
 *         description: Internal Server Error
 */
movieRoutes.post(
    '/update-movie',
    [
        body('title')
            .trim()
            .notEmpty().withMessage('There should be a movie title'),
        body('genres')
            .isArray({ min: 1 }).withMessage('Please, select at least one movie genre')
            .notEmpty().withMessage('Please, select a movie genre'),
        body('release')
            .notEmpty().withMessage('Please, select a release date')
            .isDate(),
        body('description')
            .trim()
            .notEmpty().withMessage('There should be a movie description')
            .isLength({min: 10}).withMessage('The length of description should be at least 10 charecters long')
    ],
    postUpdateMovie);
    
/**
 * @openapi
 * /movies/delete-movie:
 *   post:
 *     tags:
 *      - Movies
 *     summary: Delete movie by Id
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               movieId:
 *                 type: string
 *     description: ID of the movie to delete
 *     responses:
 *       '200':
 *         description: Ok
 *         content:
 *          text/plain:
 *             schema::
 *              type: string
 *              example: Movie successfully deleted
 *       '404':
 *         description: Movie not found
 *       '500':
 *         description: Internal Server Error
 */
movieRoutes.post('/delete-movie', postDeleteMovie);

/**
 * @openapi
 * /movies/genre/{genreId}:
 *   get:
 *     tags:
 *      - Movies
 *     summary: Filter movies by genre
 *     parameters:
 *     - name: genreId
 *       in: path
 *     schema:
 *         type: string
 *     required: true
 *     description: Genre Id
 *     responses:
 *       200:
 *         description: Ok
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                  $ref: '#/components/schemas/Movie'
 *       '404':
 *         description: Selected genre was not found
 *       '500':
 *         description: Internal Server Error
 */
movieRoutes.get('/genre/:genreId', getMovieByGenre);



export default movieRoutes;