// Set up Swagger-jsdoc

import swaggerJsdoc from 'swagger-jsdoc';

const options = {
    definition: {
        openapi: '3.0.0',
        info: {
            title: 'Homework API',
            version: '1.0.0',
        },
        components: {
            schemas: {
                Movie: {
                    type: 'object',
                    properties: {
                        title: {type: 'string'},
                        description: {type: 'string'},
                        release: {type: 'string', format: 'date'},
                        genres: {
                            type: 'array',
                            items: { $ref: '#/components/schemas/Genre'}
                        }
                    }
                },
                Genre: {
                    type: 'object',
                    properties: {
                        _id: {type: 'string'},
                        name: {type: 'string'},
                        description: {type: 'string'}
                    }
                }
            },
            testing: {
                title: 'Testing Information',
                content: `
                    <h2>Running Tests</h2>
                    <p>information on how to run tests</p>
                `,
            }
        },
        
    },
    apis: ['./src/routes/index.ts', './src/routes/movie-routes.ts', './src/routes/genre-routes.ts'],    
};

export const swaggerSpec = swaggerJsdoc(options);
