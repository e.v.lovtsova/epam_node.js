import request from 'supertest';
import app from '../app';
import { ObjectId } from 'mongodb';

const testMovieID = '6543768afc6318bc8a66df70';

// Read - get movies
describe('GET /movies', () => {
    test('should return array of available movies', async () => {
        const response = await request(app).get('/movies');
        const movie = response.body[0];

        expect(response.status).toBe(200);
        expect(Array.isArray(response.body)).toBe(true);
        expect(response.body.length).toBeGreaterThanOrEqual(1);
        expect(movie).toHaveProperty('_id');
    });
});

// Create - post movie
describe('POST /movies/add-movie', () => {
    const newMovie = {
        _id: new ObjectId(`${testMovieID}`),
        title: 'Test Movie',
        description: 'Test Description',
        release: '2023-10-07',
        genres: [
            {
                _id: '653275337ffede226b2ffb3a',
                name: 'Action film',
            },
        ],
    };

    test('should create new movie', async () => {
        const response = await request(app)
            .post('/movies/add-movie')
            .send(newMovie);

        expect(response.status).toBe(201);
        expect(response.body.title).toBe(newMovie.title);
        expect(response.body.description).toBe(newMovie.description);
    });

    test('should hendle validation errors', async () => {
        const movieEmptyData = { ...newMovie, title: '' };
        const response = await request(app)
            .post('/movies/add-movie')
            .send(movieEmptyData);

        expect(response.status).toBe(422);
        expect(response.body.message).toBe(
            'Validation failed, There should be a movie title'
        );
    });
});

// Read - get movie by Id
describe('GET /movies/:movieId', () => {
    test('should return movie by its Id', async () => {
        try {
            const response = await request(app).get(`/movies/${testMovieID}`);

            expect(response.status).toBe(200);
            expect(response.body._id).toBe(`${testMovieID}`);
        } catch (err) {
            console.error(err);
        }
    });

    test('should return error if couldnt find movie with such id', async () => {
        const response = await request(app).get('/movies/6543768afc6318bc8a66df71');
        expect(response.status).toBe(404);
        expect(response.body.message).toBe('Could not find movie.');
    });
});

// Update - put movie:id
describe('PUT /movies/update-movie', () => {
    const updatedMovie = {
        movieId: `${testMovieID}`,
        title: 'New Test Movie',
        description: 'New Test Movie Description',
        release: '2023-10-07',
        genres: [
            {
                _id: '653275337ffede226b2ffb3a',
                name: 'Action film',
            },
        ],
    };
    test('should return updated movie', async () => {
        const response = await request(app)
            .post('/movies/update-movie')
            .send(updatedMovie);

        expect(response.status).toBe(200);
        expect(response.body.title).toBe(updatedMovie.title);
        expect(response.body.description).toBe(updatedMovie.description);
        expect(response.body._id).toBe(updatedMovie.movieId);
    });

    test('should hendle validation errors', async () => {
        const updatedEmptyMovie = { ...updatedMovie, title: '' };
        const response = await request(app)
            .post('/movies/update-movie')
            .send(updatedEmptyMovie);

        expect(response.status).toBe(422);
        expect(response.body.message).toBe(
            'Validation failed, There should be a movie title'
        );
    });

    test('should return error if couldnt find movie with such id', async () => {
        const response = await request(app)
            .post('/movies/update-movie')
            .send({ ...updatedMovie, movieId: '6543768afc6318bc8a66df71' });
        expect(response.status).toBe(404);
        expect(response.body.message).toBe('Could not find movie.');
    });
});

// Delete - delete movie:id
describe('DELETE /movies/delete-movie', () => {
    const movieId = {
        movieId: `${testMovieID}`,
    };
    test('should delete selected movie', async () => {
        const response = await request(app)
            .post('/movies/delete-movie')
            .send(movieId);

        expect(response.status).toBe(200);
        expect(response.body).toBe('Movie successfully deleted');
    });

    test('should return error if movie wasn\'t found', async () => {
        const response = await request(app)
            .post('/movies/delete-movie')
            .send(movieId);

        expect(response.status).toBe(404);
        expect(response.body.message).toBe('Could not find movie.');
    });
});
