import request from 'supertest';
import app from '../app';

// GET movie by genre /movies/genre/{genreId}
describe('GET /movies/genre/:genreId', () => {
    test('should return two Historical movies', async () => {
        const historicalGenreId = '653170b3af4deb10b639a9cf';
        const response = await request(app)
            .get(`/movies/genre/${historicalGenreId}`);
        expect(response.status).toBe(200);
        expect(Array.isArray(response.body)).toBe(true);
        expect(response.body.length).toEqual(2);
    });

    test('should return three Fantasy movies', async () => {
        const fantasyGenreId = '653170c4af4deb10b639a9d2';
        const response = await request(app)
            .get(`/movies/genre/${fantasyGenreId}`);
        expect(response.status).toBe(200);
        expect(Array.isArray(response.body)).toBe(true);
        expect(response.body.length).toEqual(3);
    });

    test('should return three Drama movies', async () => {
        const dramaGenreId = '653170d1af4deb10b639a9d5';
        const response = await request(app)
            .get(`/movies/genre/${dramaGenreId}`);
        expect(response.status).toBe(200);
        expect(Array.isArray(response.body)).toBe(true);
        expect(response.body.length).toEqual(3);
    });

    test('should return three Science Fiction movies', async () => {
        const fictionGenreId = '653277b47ffede226b2ffb46';
        const response = await request(app)
            .get(`/movies/genre/${fictionGenreId}`);
        expect(response.status).toBe(200);
        expect(Array.isArray(response.body)).toBe(true);
        expect(response.body.length).toEqual(3);
    });

    test('should return one Adventure movie', async () => {
        const adventureGenreId = '6532770d7ffede226b2ffb40';
        const response = await request(app)
            .get(`/movies/genre/${adventureGenreId}`);
        expect(response.status).toBe(200);
        expect(Array.isArray(response.body)).toBe(true);
        expect(response.body.length).toEqual(1);
    });

    test('should return 404-status and error-message in case of incorrect Id', async () => {
        const incorrectId = '6532770d7ffede226b2ffb4a';
        const response = await request(app)
            .get(`/movies/genre/${incorrectId}`);
        expect(response.status).toBe(404);
        expect(response.body.message).toBe('Selected genre was not found');
    });

    test('should return an appropriate message and 200-status if there are no films of the selected genre', async () => {
        const thrillerGenreId = '653277d57ffede226b2ffb49';
        const response = await request(app)
            .get(`/movies/genre/${thrillerGenreId}`);
        expect(response.status).toBe(200);
        expect(response.body).toBe('There are no movies of selected genre at the moment');
    });
});