import mongoose from 'mongoose';
import request from 'supertest';
import dotenv from 'dotenv';
import './genre-routes';
import './movie-routes';
import './movie-search';
import app from '../app';

dotenv.config();
const uri = process.env.DB_URI;
if (!uri) {
    process.exit(1);
}
beforeEach(async() => {
    await mongoose.connect(uri);
});
afterEach(async() => {
    await mongoose.connection.close();
});

describe('GET aaditional routes', () => {
    test('should return welcome message', async () => {
        const response = await request(app).get('/');
        expect(response.status).toBe(200);
        expect(response.body.message).toBe('Welcome to home page!');
    });

    test('should load health-check data', async () => {
        const response = await request(app).get('/health-check');
        expect(response.status).toBe(200);
        expect(response.body.message).toBe('Server is running');
    });

    test('should return 404 error for', async () => {
        const response = await request(app).get('/not-found');
        expect(response.status).toBe(404);
        expect(response.body.message).toBe('The url you are trying to reach is not hosted on our server');
    });
});