import request from 'supertest';
import app from '../app';
import { ObjectId } from 'mongodb';

const testGenreID = '6543768afc6318bc8a66df70';

// Read - get genres
describe('GET /genres', () => {      
    test('should return a list of available genres', async () => {
        try {
            const response = await request(app).get('/genres');

            expect(response.status).toBe(200);
            expect(Array.isArray(response.body)).toBe(true);
            expect(response.body.length).toBeGreaterThan(0);
            expect(response.body[0]).toHaveProperty('name');
            expect(response.body[0]).toHaveProperty('description');
            expect(response.body[0]).toHaveProperty('_id');
        } catch (err) {
            console.error(err);
        }         
    });
});

// Create - post genre
describe('POST /genres/add-genre', () => {
    const genreData = {
        _id: new ObjectId(`${testGenreID}`),
        name: 'Test Genre',
        description: 'Test genre description'
    };
    test('should create a new genre', async () => {
        const response = (await request(app)
            .post('/genres/add-genre')
            .send(genreData));            

        expect(response.status).toBe(201);
        expect(response.body.name).toBe(genreData.name);
        expect(response.body.description).toBe(genreData.description);
    });

    test('should handle validation errors', async () => {
        const genreEmptyData = {...genreData, name: ''};
        const response = await request(app)
            .post('/genres/add-genre')
            .send(genreEmptyData);

        expect(response.status).toBe(422);
        expect(response.body.message).toBe('Validation failed, There should be a name of genre');
    });
});

// GET genre by Id
describe('GET /genres/:genreId', () => {
    test('should return genre by its Id', async () => {
        try {
            const response = await request(app)
                .get(`/genres/${testGenreID}`);

            expect(response.status).toBe(200);
            expect(response.body._id).toBe(`${testGenreID}`);
        } catch (err) {
            console.error(err);
        }
    });

    test('should return error if couldnt find genre with such id', async () => {
        try {
            const response = await request(app)
                .get('/genres/653170e4af4deb10b639a9d9');

            expect(response.status).toBe(404);
            expect(response.body.message).toBe('Genre not found');
        } catch (err) {
            console.error(err);
        }
    });
});

// Update - put genre:id
describe('PUT /genres/update-genre', () => {
    const updatedGenre = {
        'genreId': `${testGenreID}`,
        'name': 'New Test Genre',
        'description': 'New Test Genre Description'
    };
    test('should return updated genre', async () => {
        const response = await request(app)
            .post('/genres/update-genre')
            .send(updatedGenre);

        expect(response.status).toBe(200);
        expect(response.body.name).toBe(updatedGenre.name);
        expect(response.body.description).toBe(updatedGenre.description);
    });

    test('should handle validation errors', async () => {
        const updatedEmptyGenre = {...updatedGenre, name: ''};
        const response = await request(app)
            .post('/genres/update-genre')
            .send(updatedEmptyGenre);

        expect(response.status).toBe(422);
        expect(response.body.message).toBe('Validation failed, There should be a name of genre');
    });

    test('should return error if couldnt find genre with such id', async () => {
        const response = await request(app)
            .post('/genres/update-genre')
            .send({...updatedGenre, genreId: '653170e4af4deb10b639a9d9' });
        expect(response.status).toBe(404);
        expect(response.body.message).toBe('Genre not found');
    });
});

// Delete - delete genre:id
describe('DELETE /genres/delete-genre', () => {
    test('should delete selected genre', async() => {
        const response = await request(app)
            .post('/genres/delete-genre')
            .send({genreId: `${testGenreID}`});

        expect(response.status).toBe(200);
        expect(response.body).toBe('Genre successfully deleted');
    });

    test('should return error if genre wasn\'t found', async () => {
        const response = await request(app)
            .post('/genres/delete-genre')
            .send({genreId: `${testGenreID}`});

        expect(response.status).toBe(404);
        expect(response.body.message).toBe('Genre not found');
    });
});
