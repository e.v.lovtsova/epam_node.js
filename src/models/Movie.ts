import mongoose, { Schema, Types } from 'mongoose';
import { IGenre } from './Genre';

interface IMovie {
    title: string;
    description: string;
    release: Date;
    genres: Types.ObjectId[] | IGenre[]
}

const movieSchema = new Schema<IMovie>({
    title: {
        type: Schema.Types.String,
        required: true,
    },
    description: {
        type: Schema.Types.String,
        required: true,
    },
    release: {
        type: Schema.Types.Date,
        required: true,
    },
    genres: [{
        type: Schema.Types.ObjectId,
        ref: 'Genre'
    }]
});

export const MovieModel = mongoose.model<IMovie>('Movie', movieSchema);