import mongoose, { Schema } from 'mongoose';

export interface IGenre {
    name: string;
    description: string
}

const genreSchema = new Schema<IGenre>({
    name: {
        type: String,
        required: true,
    },
    description: {
        type: String,
        required: true
    }
});

export const GenreModel = mongoose.model<IGenre>('Genre', genreSchema);