import express, { Request, Response, NextFunction } from 'express';
import swaggerUi from 'swagger-ui-express';

import { swaggerSpec } from './swaggerSetup';
import routes from './routes';
import CustomError from './utils/customError';

const app = express(); 

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));
app.use('/', routes);

app.use((req: Request, res: Response, next: NextFunction) => {
    res.status(404).json({ message: 'The url you are trying to reach is not hosted on our server' });
    next();
});

app.use((err: CustomError, req: Request, res: Response, next: NextFunction) => {
    const status = err.status || 500;
    const message = err.message || 'Internal Server Error';
    res.status(status).json({ status: status, message: message });
});


export default app;