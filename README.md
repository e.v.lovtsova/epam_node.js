# SERVER DOCUMENTATION

### START

1. npm install
2. npm run start
3. start your ui

### MODEL USING IN PROJECT

### Genre model

```
{
    name: string;
    description: string
}
```

### Movie model

```
{
    title: string;
    description: string;
    release: Date;
    genres: GENRE_MODEL[]
}
```

## Running Tests with Jest

To run the tests for this project, use the following command:

```
npm run test
```

This command will start Jest and execute all the test cases in the project.

## Interpreting Test Results

When you run `npm run test`, Jest will provide output indicating whether the tests have passed or failed. If a test fails, Jest will display an error message indicating what went wrong. Make sure to carefully read the error message to understand the issue. You can then go back to code to fix the problem and re-run the tests.

## Viewing Test Coverage

After running the tests, a folder named `coverage` will be generated. This folder contains detailed reports about the code coverage of your tests.

To view the coverage report in your browser, follow these steps:

1. Open the `coverage` folder.
2. Locate the `index.html` file inside the `coverage` folder.
3. Double-click on `index.html` to open it in your preferred web browser.

This will display a detailed report showing which parts of code are covered by tests and which are not.

---
